﻿namespace OCRConverterTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using System.Collections.Generic;
    using System.Linq;
    using BankOCR_v2;

    [TestClass]
    public class OCRConverterTests
    {
        private static readonly List<OCRZiffer> ZiffernVorrat = new List<OCRZiffer>
        {
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Leer, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Vertikal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Vertikal, OCRStrich.Leer }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Leer }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }),
            new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal }
                })
        };

        [TestMethod]
        public void ConverterTest()
        {
            OCRZahl alleZahlen = new OCRZahl(ZiffernVorrat);
            OCRZahl inputZahl = alleZahlen;

            var ocrConverter = new OCRConverter();

            // Test mit allen Zahlen 0123456789
            var output = ocrConverter.Identifizieren(new OCRZahl[] { inputZahl });
            Assert.IsNotNull(output, "Converter returned null");
            Assert.AreEqual(1, output.Count(), "Converter did not return 1 string");
            Assert.AreEqual("0123456789", output.First(), "Converter did not return the expected");

            // Test mit Zahl 153
            inputZahl = new OCRZahl(new OCRZiffer[] { ZiffernVorrat[1], ZiffernVorrat[5], ZiffernVorrat[3] });
            output = ocrConverter.Identifizieren(new OCRZahl[] { inputZahl });
            Assert.IsNotNull(output, "Converter returned null");
            Assert.AreEqual(1, output.Count(), "Converter did not return 1 string");
            Assert.AreEqual("153", output.First(), "Converter did not return the expected");

            // Test mit Zahl 153 und allen Zahlen,
            inputZahl = new OCRZahl(new OCRZiffer[] { ZiffernVorrat[1], ZiffernVorrat[5], ZiffernVorrat[3] });
            output = ocrConverter.Identifizieren(new OCRZahl[] { inputZahl, alleZahlen });
            Assert.IsNotNull(output, "Converter returned null");
            Assert.AreEqual(2, output.Count(), "Converter did not return 2 strings");
            Assert.AreEqual("153", output.ElementAt(0), "Converter did not return the expected");
            Assert.AreEqual("0123456789", output.ElementAt(1), "Converter did not return the expected");
        }
    }
}
