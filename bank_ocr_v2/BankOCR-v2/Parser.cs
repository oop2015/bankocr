﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace BankOCR_v2
{
    using System.Linq;

    class Parser
    {
        public IEnumerable<OCRZahl> Parse(string ocrString)
        {
            IEnumerable<string[]> zeilenDreier = this.ZeilenGruppieren(ocrString);
            return this.SpaltenGruppieren(zeilenDreier);
        }
   
        private Collection<string[]> ZeilenGruppieren(string ocrString)
        {
            Collection<string[]> zeilen3er = new Collection<string[]>();

            using (var sr = new StringReader(ocrString))
            {
                string akutelleZeile = sr.ReadLine();
                while (akutelleZeile != null)
                {
                    string[] zeile3er = new string[3];
                    zeile3er[0] = akutelleZeile;
                    zeile3er[1] = sr.ReadLine();
                    zeile3er[2] = sr.ReadLine();
                    zeilen3er.Add(zeile3er);

                    akutelleZeile = sr.ReadLine();
                    akutelleZeile = sr.ReadLine();
                }
            }

            return zeilen3er;
        }

        private IEnumerable<OCRZahl> SpaltenGruppieren(IEnumerable<string[]> zeilenDreier)
        {
            var zahlenweiseZiffern = zeilenDreier.Select(this.ZiffernLesen);

            return zahlenweiseZiffern.Select(ziffern => new OCRZahl(ziffern));
        }

        private Collection<OCRZiffer> ZiffernLesen(string[] zeileDreier)
        {
            Collection<OCRZiffer> ocrZiffern = new Collection<OCRZiffer>();
            for (int i = 0; i < zeileDreier[0].Length; i = i+4)
            {
                OCRStrich[] stricheFuerZeile1 = this.ToStriche(zeileDreier[0].Substring(i, 3).ToCharArray());
                OCRStrich[] stricheFuerZeile2 = this.ToStriche(zeileDreier[1].Substring(i, 3).ToCharArray());
                OCRStrich[] stricheFuerZeile3 = this.ToStriche(zeileDreier[2].Substring(i, 3).ToCharArray());
                OCRStrich[,] striche = { { stricheFuerZeile1[0], stricheFuerZeile1[1], stricheFuerZeile1[2] }, { stricheFuerZeile2[0], stricheFuerZeile2[1], stricheFuerZeile2[2] }, { stricheFuerZeile3[0], stricheFuerZeile3[1], stricheFuerZeile3[2] } };
                ocrZiffern.Add(new OCRZiffer(striche));
            }

            return ocrZiffern;
        }

        private OCRStrich[] ToStriche(char[] s)
        {
            OCRStrich[] striche = new OCRStrich[s.Length];
            {
                for (int i = 0; i < s.Length; i++)
                {
                    if (s[i] == '_')
                    {
                        striche[i] = OCRStrich.Horizontal;
                    }
                    else if (s[i] == 'I')
                    {
                        striche[i] = OCRStrich.Vertikal;
                    }
                    else
                    {
                        striche[i] = OCRStrich.Leer;
                    }
                }
            }

            return striche;
        }
    }
}
