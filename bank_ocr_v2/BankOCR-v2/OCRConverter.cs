namespace BankOCR_v2
{
    #region using directives

    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    #endregion

    internal class OCRConverter
    {
        private static readonly Dictionary<OCRZiffer, char> ZiffernVorrat = new Dictionary<OCRZiffer, char>(new OCRZifferComparer())
        {
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Leer, OCRStrich.Vertikal },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }), '0' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal }
                }), '1' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Leer }
                }), '2' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }), '3' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal }
                }), '4' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }), '5' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }), '6' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Leer, OCRStrich.Vertikal }
                }), '7' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }), '8' 
            },
            { new OCRZiffer(new[,]
                {
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Leer },
                    { OCRStrich.Vertikal, OCRStrich.Horizontal, OCRStrich.Vertikal },
                    { OCRStrich.Leer, OCRStrich.Horizontal, OCRStrich.Vertikal }
                }), '9' 
            }
        };

        internal IEnumerable<string> Identifizieren(IEnumerable<OCRZahl> ocrZahlen)
        {
            foreach (var ocrZahl in ocrZahlen)
            {
                StringBuilder zahlBuilder = new StringBuilder();
                foreach (var ocrZiffer in ocrZahl.Ziffern)
                {
                    zahlBuilder.Append(this.OcrZifferBehandeln(ocrZiffer));
                }

                yield return zahlBuilder.ToString();
            }
        }

        private char OcrZifferBehandeln(OCRZiffer ocrZiffer)
        {
            return ZiffernVorrat[ocrZiffer];
        }

        private class OCRZifferComparer : IEqualityComparer<OCRZiffer>
        {
            public bool Equals(OCRZiffer x, OCRZiffer y)
            {
                if (object.ReferenceEquals(x, null))
                {
                    return object.ReferenceEquals(y, null);
                }

                if (object.ReferenceEquals(y, null))
                {
                    return false;
                }

                return this.OcrZiffernStrichFuerStrichVergleichen(x, y);
            }

            private bool OcrZiffernStrichFuerStrichVergleichen(OCRZiffer x, OCRZiffer y)
            {
                for (int i = 0; i < x.Striche.GetLength(0); ++i)
                {
                    for (int j = 0; j < x.Striche.GetLength(1); ++j)
                    {
                        if (x.Striche[i, j] != y.Striche[i, j])
                        {
                            return false;
                        }
                    }
                }

                return true;
            }

            public int GetHashCode(OCRZiffer obj)
            {
                if (object.ReferenceEquals(obj, null))
                {
                    return 0;
                }

                return obj.Striche.Cast<OCRStrich>().Aggregate(0, (current, ocrStrich) => current ^ ocrStrich.GetHashCode());
            }
        }
    }
}
