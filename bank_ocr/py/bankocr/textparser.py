def zeilen_gruppieren(lines):
    result, group = [], []
    for i, line in enumerate(lines):
        if i % 4 == 3:
            continue
        group.append(line)
        if i % 4 == 2:
            result.append(tuple(group))
            group = []
    if len(group) == 3:
        result += group
    return result


def ziffern_gruppieren(line_triplets):
    clusters = []
    for trip in line_triplets:
        if len(trip) != 3:
            print "Skipped invalid block"
            continue
        if len(trip[0]) != len(trip[1]) or len(trip[0]) != len(trip[2]):
            print "Skipped invalid block, check your line endings"
            continue
        # FIXME This is horrible to read, sorry
        cluster = None
        for i in xrange(3, len(trip[0]) + 1, 4):
            cluster = (trip[0][i - 3:i], trip[1][i - 3:i], trip[2][i - 3:i])
            clusters.append(cluster)
    return clusters

if __name__ == '__main__':
    #test = zeilen_gruppieren(['   ', '  I', '  I'])
    #test = [('a', 'b', 'c')]
    # print test
    # quit()
    test = zeilen_gruppieren(['a', 'b', 'c', '', 'd', 'e', 'f'])
    assert test == [('a', 'b', 'c'), ('d', 'e', 'f')]
    test = zeilen_gruppieren(['a', 'b', 'c', 'd', 'e', 'f'])
    assert test == [('a', 'b', 'c')]
    test = zeilen_gruppieren(['a', 'b'])
    assert test == []
    test = zeilen_gruppieren([])
    assert test == []
    test = ziffern_gruppieren([('ABC DEF', 'ABC DEF', 'ABC DEF')])
    assert test == [('ABC', 'ABC', 'ABC'), ('DEF', 'DEF', 'DEF')]
    test = ziffern_gruppieren([('ABC DEF ', 'ABC DEF ', 'ABC DEF ')])
    assert test == [('ABC', 'ABC', 'ABC'), ('DEF', 'DEF', 'DEF')]
    test = ziffern_gruppieren([('ABC DE', 'ABC DE', 'ABC DE')])
    assert test == [('ABC', 'ABC', 'ABC')]
