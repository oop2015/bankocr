import argparse
import os.path


def dateinamen_ermitteln():
    # Difference from spec - Python doesn't need to pass around args from main
    # This function does some extra work, this should be moved elsewhere
    parser = argparse.ArgumentParser(
        description='Convert ASCII Text to integers, as per "BankOCR" rules..')
    parser.add_argument('filename', metavar='filename', type=str,
                        help='a filename to process')
    args = parser.parse_args()
    return args.filename


def zeilen_ausgeben(str_list):
    for line in str_list:
        print line
