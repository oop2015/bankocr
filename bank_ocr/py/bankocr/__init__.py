from cli import dateinamen_ermitteln, zeilen_ausgeben
from textprovider import datei_einlesen
from ocr import ziffern_identifizieren
from textparser import zeilen_gruppieren, ziffern_gruppieren
