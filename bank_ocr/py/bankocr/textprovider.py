import os.path


def datei_einlesen(filename):
    if not os.path.isfile(filename):
        print "File '%s' not found" % (filename)
        return None
    with open(filename, "r") as f:
        str_list = [line.replace('\n', '') for line in f.readlines()]
    return str_list
