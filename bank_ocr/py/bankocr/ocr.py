def ziffern_identifizieren(cluster_list):
    mapping = {
        ('     I  I'): '1',
        (' _  _II_ '): '2',
        (' _  _I _I'): '3',
        ('   I_I  I'): '4',
        (' _ I_  _I'): '5',
        (' _ I_ I_I'): '6',
        (' _   I  I'): '7',
        (' _ I_II_I'): '8',
        (' _ I_I _I'): '9',
        (' _ I II_I'): '0',
    }
    result = ""
    for cluster in cluster_list:
        cluster_str = "".join(cluster)
        v = ocr(cluster_str, mapping)
        if type(v) == str:
            result += v
    return result


def ocr(cluster_str, mapping):
    if cluster_str in mapping:
        return mapping[cluster_str]
    return None

if __name__ == '__main__':
    assert ziffern_identifizieren([('   ', '  I', '  I')]) == "1"
