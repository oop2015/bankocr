import bankocr

if __name__ == '__main__':
    filename = bankocr.cli.dateinamen_ermitteln()
    filecontent = bankocr.textprovider.datei_einlesen(filename)
    triplets = bankocr.textparser.zeilen_gruppieren(filecontent)
    cluster = bankocr.textparser.ziffern_gruppieren(triplets)
    result = bankocr.ocr.ziffern_identifizieren(cluster)
    bankocr.cli.zeilen_ausgeben([result])
