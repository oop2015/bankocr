﻿using System.Collections.Generic;

namespace BankOCR_v2
{
    class OCRZahl
    {
        public OCRZahl(IEnumerable<OCRZiffer> ziffern)
        {
            this.Ziffern = ziffern;
        }
        
        public IEnumerable<OCRZiffer> Ziffern
        {
            get;
            private set;
        }
    }

    internal class OCRZiffer
    {
        public OCRZiffer(OCRStrich[,] striche)
        {
            this.Striche = striche;
        }

        public OCRStrich[,] Striche
        {
            get;
            private set;
        }
    }

    internal enum OCRStrich
    {
        Leer,

        Vertikal,

        Horizontal
    }
}
