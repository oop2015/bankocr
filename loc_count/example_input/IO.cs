﻿namespace BankOCR_v2
{
    using System;
    using System.Collections.Generic;

    class IO
    {
        public string laden(string[] args)
        {
           var dateiname = dateiname_ermitteln(args);
          return datei_einlesen(dateiname);
        }

        public void ausgeben(IEnumerable<string> zahlen)
        {
            foreach (var zahl in zahlen)
            {
                Console.WriteLine(zahl);
            }
        }

        private static string dateiname_ermitteln(string[] args)
        {
            return args[0];
        }

        private static string datei_einlesen(string dateiname)
        {
            return System.IO.File.ReadAllText(dateiname);
        }
    }
}
