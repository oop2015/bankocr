﻿namespace BankOCR_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            var io = new IO();
            var parser = new Parser();
            var ocrConverter = new OCRConverter();

            var fileImString = io.laden(args);
            var ocrZahlenJeZeile = parser.Parse(fileImString);
            var konvertierteZahlenJeZeile = ocrConverter.Identifizieren(ocrZahlenJeZeile);
            io.ausgeben(konvertierteZahlenJeZeile);
        }
    }
}
