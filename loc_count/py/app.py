import os


def list_files_of_type(path, filetype):
    filenames = []
    for dirpath, _, files in os.walk(path):
        for filename in files:
            if not filename.endswith(filetype):
                continue
            filenames.append(os.path.join(dirpath, filename))
    return filenames

path = "../example_input"

for f in list_files_of_type(path, ".cs"):
    print f
