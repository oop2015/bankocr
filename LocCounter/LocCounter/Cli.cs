﻿namespace LocCounter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class Cli
    {
        public void Ausgeben(int anzahlDateien, int loc)
        {
            Console.WriteLine("Anzahl Dateien: {0}  LOC: {1}", anzahlDateien, loc);
        }

        public void args_parsen(string[] args, out string pfad, out Dateifilter dateifilter)
        {
            pfad = args[0];

            var includes = args.Where(a => a.StartsWith("+")).Select(a => a.TrimStart('+'));
            var excludes = args.Where(a => a.StartsWith("-")).Select(a => a.TrimStart('-')); 

            dateifilter = new Dateifilter(includes, excludes);
        }
    }
}
