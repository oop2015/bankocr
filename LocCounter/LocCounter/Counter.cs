﻿namespace LocCounter
{
    class Counter
    {
        private int zähler;

        public void Zählen()
        {
            this.zähler++;
        }

        public int LocAuslesen()
        {
            return this.zähler;
        }
    }
}
