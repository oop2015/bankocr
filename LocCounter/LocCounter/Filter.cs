﻿namespace LocCounter
{
    using System;

    class Filter
    {
        private bool inKommentar = false;

        public void leerzeilen_entfernen(string zeile, Action<string> weitergeben)
        {
            if (!String.IsNullOrWhiteSpace(zeile))
            {
                weitergeben(zeile);
            }
        }

        public void einzeilige_kommentaren_entfernen(string zeile, Action<string> weitergeben)
        {
            if (!zeile.TrimStart().StartsWith("//"))
            {
                weitergeben(zeile);
            }
        }

        public void mehrzeilige_kommentaren_entfernen(string zeile, Action<string> weitergeben)
        {
            this.kommentaranfang_behandeln(zeile,
                weitergeben,
                z => this.kommentarende_behandeln(z,
                        weitergeben));
        }

        private void kommentaranfang_behandeln(string zeile, Action<string> weitergeben, Action<string> nichtZustaendig)
        {
            if (this.inKommentar)
            {
                nichtZustaendig(zeile);
                return;
            }

            string[] getrennt = zeile.Split(new[] { "/*" }, StringSplitOptions.None);

            if (getrennt.Length > 1)
            {
                this.inKommentar = true;
            }

            if (!String.IsNullOrWhiteSpace(getrennt[0]))
            {
                weitergeben(zeile);
            }
        }

        private void kommentarende_behandeln(string zeile, Action<string> weitergeben)
        {
            if (!this.inKommentar)
            {
                throw new InvalidOperationException("Kann Ende nicht suchen wenn ich nicht im Kommentar bin");
            }

            string[] getrennt = zeile.Split(new[] { "*/" }, StringSplitOptions.None);

            if (getrennt.Length > 1)
            {
                this.inKommentar = false;
                if (!String.IsNullOrWhiteSpace(getrennt[getrennt.Length - 1]))
                {
                    weitergeben(zeile);
                }
            }
        }
    }
}
