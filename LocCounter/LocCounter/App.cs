﻿namespace LocCounter
{
    using System;

    class App
    {
        private Counter counter = new Counter();

        private Cli cli = new Cli();

        internal void Run(string[] args)
        {
            this.QuellcodeLaden(args, 
                this.ZeilenAnalysieren, 
                anzahlDateien =>
                {
                    var loc = this.counter.LocAuslesen();
                    this.cli.Ausgeben(anzahlDateien, loc);
                });
        }

        // Einzeiliger Kommentar

        private void QuellcodeLaden(string[] args, Action<string> beiZeile, Action<int> beiAnzahlDateien)
        {
            string wurzelpfad;
            Dateifilter dateifilter;
            this.cli.args_parsen(args, out wurzelpfad, out dateifilter); // einzeiliger Kommentar am Ende eines Ausdrucks

            /* Mehr-
             * zeiliger
             * Kommentar */

            var dateiSystem = new Dateisystem();

            dateiSystem.RekursivDateienSuchen(wurzelpfad, // einzeilig mittendrin */
                pfad => dateifilter.GewuenschteDateienErmitteln(pfad,
                        pfad2 => dateiSystem.DateiLaden(pfad2, 
                                    beiAnzahlDateien,
                                    beiZeile)));
        }

        private void ZeilenAnalysieren(string zeile)
        {
            var filter = new Filter();
            filter.leerzeilen_entfernen(zeile,
                    zeile2 => filter.einzeilige_kommentaren_entfernen(zeile2,
                                zeile3 => filter.mehrzeilige_kommentaren_entfernen(zeile3,
                                            _ => this.counter.Zählen())));
        }
    }
}
