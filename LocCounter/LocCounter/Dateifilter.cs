﻿using System;
using System.Collections.Generic;

namespace LocCounter
{
    using System.Linq;

    class Dateifilter
    {
        private readonly IEnumerable<string> includes;

        private readonly IEnumerable<string> excludes;

        public Dateifilter(IEnumerable<string> includes, IEnumerable<string> excludes)
        {
            this.includes = includes;
            this.excludes = excludes;
        }

        internal void GewuenschteDateienErmitteln(string dateiPfad, Action<string> beiErmitteltemDateipfad)
        {
            if (dateiPfad == null)
            {
                beiErmitteltemDateipfad(null);
                return;
            }

            if (this.excludes.Any(exclude => dateiPfad.IndexOf(exclude, StringComparison.OrdinalIgnoreCase) >= 0))
            {
                return;
            }

            if (this.includes.Any(include => dateiPfad.IndexOf(include, StringComparison.OrdinalIgnoreCase) >= 0))
            {
                beiErmitteltemDateipfad(dateiPfad);
            }
        }
    }
}
