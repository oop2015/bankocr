﻿using System;
using System.IO;

namespace LocCounter
{
    class Dateisystem
    {
        int fileCount = 0;
        
        internal void RekursivDateienSuchen(string pfad, Action<string> beiDateiPfad)
        {
            var dateien = Directory.GetFiles(pfad, "*.*", SearchOption.AllDirectories);

            foreach (var datei in dateien)
            {
                beiDateiPfad(datei);
            }

            beiDateiPfad(null);
        }

        

        internal void DateiLaden(string dateiPfad, Action<int> beiAnzahlFiles, Action<string> beiZeile)
        {
            if(dateiPfad == null)
            {
                beiAnzahlFiles(this.fileCount);
                return;
            }

            var zeilen = File.ReadAllLines(dateiPfad);
            this.fileCount++;

            foreach (var zeile in zeilen)
            {
                beiZeile(zeile);
            }
        }
    }
}
