﻿namespace NBack
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Auswertung
    {
        public static Ergebnis ErgebnisBerechnen(Protokoll protokoll)
        {
            var musterLoesung = Auswertung.MusterloesungBerechnen(protokoll.Testfolge, Convert.ToInt32(protokoll.N));
            return Auswertung.LoesungenVergleichen(protokoll.Antworten.ToList(), musterLoesung);
        }

        private static List<Antworten> MusterloesungBerechnen(Testfolge testfolge, int n)
        {
            var wiederholungen = testfolge.WiederholungenErmitteln(n);

            return wiederholungen.Select(w => w ? Antworten.Wiederholung : Antworten.KeineWiederholung).ToList();
        }

        private static Ergebnis LoesungenVergleichen(IReadOnlyList<Antworten> antworten, IReadOnlyList<Antworten> musterloesung)
        {
            double success = 0;
            double n = antworten.Count;
            for (var i = 0; i < n; i++)
            {
                if (antworten[i] == musterloesung[i])
                {
                    success++;
                }
            }

            return new Ergebnis { ProzentRichtig = 100.0 * success / n };
        }
    }
}
