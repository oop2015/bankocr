namespace NBack
{
    using System;
    using System.Text;

    public class TestfolgenGenerator
    {
        public static Testfolge TestfolgeErzeugen(UInt32 n, UInt32 anzahlDerReize)
        {
            var sb = new StringBuilder();

            for (var i = 0; i < anzahlDerReize; i++)
            {
                sb.Append(RandomLetter.GetLetter());
            }

            return new Testfolge(sb.ToString());
        }

        private static class RandomLetter
        {
            static readonly Random _random = new Random();

            public static char GetLetter()
            {
                var num = _random.Next(0, 26);
                var let = (char)('A' + num);
                return let;
            }
        }
    }
}
