﻿namespace NBack
{
    using System;

    public class App
    {
		ParamDialog paramDlg;

        Protokoll protokoll;


		public App() {
			this.paramDlg = new ParamDialog();
		    var testDlg1 = new TestDialog ();

		    this.paramDlg.ParameterEingegeben += (probandenName, n, msecReizdauer, l) => {
				TestStarten (probandenName, n, msecReizdauer, l,
					testDlg1.ZeigeFrage);
			};

			testDlg1.AntwortGegeben += antwort => {
				AntwortVerarbeiten(antwort,
					testDlg1.ZeigeFrage,
					ergebnis => {
						testDlg1.Beenden();
						this.paramDlg.ZeigeErgebnis(ergebnis);
					});
			};
		}


        public void Run()
        {
			this.paramDlg.Starten();
        }


		private void TestStarten(String probandenName, UInt32 n, UInt32 milliSekundenReizDauer, UInt32 anzahlDerReize, Action<Char> beiErsteFrage)
		{
			this.protokoll = Protokoll.Baue (probandenName, n, milliSekundenReizDauer, anzahlDerReize);
			var testfolge = TestfolgenGenerator.TestfolgeErzeugen (n, anzahlDerReize);
			this.protokoll.Testfolge = testfolge;

			char ersteFrage;
			testfolge.VersucheNaechsteFrageZuHolen (out ersteFrage);

			beiErsteFrage (ersteFrage);
		}


        private void AntwortVerarbeiten(Antworten antwort, Action<Char> beiNaechsteFrage, Action<Ergebnis> beiErgebnis)
        {
			this.protokoll.AntwortRegistrieren (antwort);

			char nächsteFrage;
			if (this.protokoll.Testfolge.VersucheNaechsteFrageZuHolen (out nächsteFrage)) {
				beiNaechsteFrage (nächsteFrage);
			} else {
				var ergebnis = Auswertung.ErgebnisBerechnen (this.protokoll);
				beiErgebnis (ergebnis);
			}
        }
    }
}
