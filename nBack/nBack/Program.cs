﻿using System;
using System.Windows.Forms;

namespace NBack
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            App app = new App();
            app.Run();
        }
    }
}
