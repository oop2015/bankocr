namespace NBack
{
    #region using directives

    using System;
    using System.Collections.Generic;
    using System.Security.Permissions;

    #endregion

    public class Protokoll
    {
        private List<Antworten> antworten = new List<Antworten>();

        private Protokoll(String probandenName, UInt32 n, UInt32 milliSekundenReizDauer, UInt32 anzahlDerReize)
        {
            this.ProbandenName = probandenName;
            this.N = n;
            this.MilliSekundenReizDauer = milliSekundenReizDauer;
            this.AnzahlDerReize = anzahlDerReize;
        }

        public string ProbandenName
        {
            get;
            private set;
        }

        public uint N
        {
            get;
            private set;
        }

        public uint MilliSekundenReizDauer
        {
            get;
            private set;
        }

        public uint AnzahlDerReize
        {
            get;
            private set;
        }

        public Testfolge Testfolge
        {
            get;
            set;
        }

        public IEnumerable<Antworten> Antworten
        {
            get
            {
                return this.antworten;
            }
        }

        public static Protokoll Baue(String probandenName, UInt32 n, UInt32 milliSekundenReizDauer, UInt32 anzahlDerReize)
        {
            return new Protokoll(probandenName, n, milliSekundenReizDauer, anzahlDerReize);
        }

        public void AntwortRegistrieren(Antworten antwort)
        {
            if (this.antworten.Count >= this.AnzahlDerReize)
            {
                throw new InvalidOperationException("Es liegen bereits genug Antworten vor");
            }

            this.antworten.Add(antwort);
        }
    }
}
