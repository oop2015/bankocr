namespace NBack
{
    #region using directives

    using System;
    using System.Collections.Generic;

    #endregion

    public class Testfolge
    {
        private String fragen;

        private int index = 0;

        public Testfolge(String fragen)
        {
            this.fragen = fragen;
        }

        public String Fragen
        {
            get
            {
                return this.fragen;
            }
        }

        public Boolean VersucheNaechsteFrageZuHolen(out Char naechsteFrage)
        {
            if (this.index < this.fragen.Length)
            {
                naechsteFrage = this.fragen[this.index];
                this.index++;
                return true;
            }

            naechsteFrage = Char.MinValue;
            return false;
        }

        public IEnumerable<bool> WiederholungenErmitteln(int n)
        {
            var wiederholungen = new List<bool>();
            var fragenLength = this.fragen.Length;
            for (var i = 0; i < fragenLength; i++)
            {
                wiederholungen.Add(i - n >= 0 && this.fragen[i] == this.fragen[i - n]);
            }

            return wiederholungen;
        }
    }
}
