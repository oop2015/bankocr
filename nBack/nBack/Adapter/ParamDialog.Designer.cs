﻿namespace NBack
{
    partial class ParamDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStarteTest = new System.Windows.Forms.Button();
            this.textBoxProbandenName = new System.Windows.Forms.TextBox();
            this.labelProband = new System.Windows.Forms.Label();
            this.numericUpDownN = new System.Windows.Forms.NumericUpDown();
            this.labelN = new System.Windows.Forms.Label();
            this.labelReizDauer = new System.Windows.Forms.Label();
            this.numericUpDownReizDauer = new System.Windows.Forms.NumericUpDown();
            this.labelReizDauerEinheit = new System.Windows.Forms.Label();
            this.labelReizZahl = new System.Windows.Forms.Label();
            this.numericUpDownAnzahlReize = new System.Windows.Forms.NumericUpDown();
            this.labelLetztesErgebnis = new System.Windows.Forms.Label();
            this.textBoxErgebnis = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownReizDauer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnzahlReize)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonStarteTest
            // 
            this.buttonStarteTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonStarteTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStarteTest.Location = new System.Drawing.Point(310, 394);
            this.buttonStarteTest.Name = "buttonStarteTest";
            this.buttonStarteTest.Size = new System.Drawing.Size(166, 48);
            this.buttonStarteTest.TabIndex = 0;
            this.buttonStarteTest.Text = "Starte Test";
            this.buttonStarteTest.UseVisualStyleBackColor = true;
            this.buttonStarteTest.Click += new System.EventHandler(this.buttonStarteTest_Click);
            // 
            // textBoxProbandenName
            // 
            this.textBoxProbandenName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxProbandenName.Location = new System.Drawing.Point(96, 29);
            this.textBoxProbandenName.Name = "textBoxProbandenName";
            this.textBoxProbandenName.Size = new System.Drawing.Size(639, 20);
            this.textBoxProbandenName.TabIndex = 1;
            // 
            // labelProband
            // 
            this.labelProband.AutoSize = true;
            this.labelProband.Location = new System.Drawing.Point(30, 32);
            this.labelProband.Name = "labelProband";
            this.labelProband.Size = new System.Drawing.Size(50, 13);
            this.labelProband.TabIndex = 2;
            this.labelProband.Text = "Proband:";
            // 
            // numericUpDownN
            // 
            this.numericUpDownN.Location = new System.Drawing.Point(96, 65);
            this.numericUpDownN.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDownN.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownN.Name = "numericUpDownN";
            this.numericUpDownN.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownN.TabIndex = 3;
            this.numericUpDownN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownN.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // labelN
            // 
            this.labelN.AutoSize = true;
            this.labelN.Location = new System.Drawing.Point(62, 67);
            this.labelN.Name = "labelN";
            this.labelN.Size = new System.Drawing.Size(18, 13);
            this.labelN.TabIndex = 4;
            this.labelN.Text = "N:";
            // 
            // labelReizDauer
            // 
            this.labelReizDauer.AutoSize = true;
            this.labelReizDauer.Location = new System.Drawing.Point(22, 102);
            this.labelReizDauer.Name = "labelReizDauer";
            this.labelReizDauer.Size = new System.Drawing.Size(58, 13);
            this.labelReizDauer.TabIndex = 6;
            this.labelReizDauer.Text = "Reizdauer:";
            // 
            // numericUpDownReizDauer
            // 
            this.numericUpDownReizDauer.Location = new System.Drawing.Point(96, 100);
            this.numericUpDownReizDauer.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numericUpDownReizDauer.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownReizDauer.Name = "numericUpDownReizDauer";
            this.numericUpDownReizDauer.Size = new System.Drawing.Size(91, 20);
            this.numericUpDownReizDauer.TabIndex = 5;
            this.numericUpDownReizDauer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownReizDauer.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // labelReizDauerEinheit
            // 
            this.labelReizDauerEinheit.AutoSize = true;
            this.labelReizDauerEinheit.Location = new System.Drawing.Point(193, 102);
            this.labelReizDauerEinheit.Name = "labelReizDauerEinheit";
            this.labelReizDauerEinheit.Size = new System.Drawing.Size(32, 13);
            this.labelReizDauerEinheit.TabIndex = 7;
            this.labelReizDauerEinheit.Text = "msec";
            // 
            // labelReizZahl
            // 
            this.labelReizZahl.AutoSize = true;
            this.labelReizZahl.Location = new System.Drawing.Point(8, 138);
            this.labelReizZahl.Name = "labelReizZahl";
            this.labelReizZahl.Size = new System.Drawing.Size(72, 13);
            this.labelReizZahl.TabIndex = 9;
            this.labelReizZahl.Text = "Anzahl Reize:";
            // 
            // numericUpDownAnzahlReize
            // 
            this.numericUpDownAnzahlReize.Location = new System.Drawing.Point(96, 136);
            this.numericUpDownAnzahlReize.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownAnzahlReize.Name = "numericUpDownAnzahlReize";
            this.numericUpDownAnzahlReize.Size = new System.Drawing.Size(91, 20);
            this.numericUpDownAnzahlReize.TabIndex = 8;
            this.numericUpDownAnzahlReize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownAnzahlReize.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelLetztesErgebnis
            // 
            this.labelLetztesErgebnis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelLetztesErgebnis.AutoSize = true;
            this.labelLetztesErgebnis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLetztesErgebnis.Location = new System.Drawing.Point(12, 182);
            this.labelLetztesErgebnis.Name = "labelLetztesErgebnis";
            this.labelLetztesErgebnis.Size = new System.Drawing.Size(150, 20);
            this.labelLetztesErgebnis.TabIndex = 10;
            this.labelLetztesErgebnis.Text = "Letztes Ergebnis:";
            this.labelLetztesErgebnis.Visible = false;
            // 
            // textBoxErgebnis
            // 
            this.textBoxErgebnis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxErgebnis.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxErgebnis.Font = new System.Drawing.Font("Tahoma", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxErgebnis.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxErgebnis.Location = new System.Drawing.Point(91, 234);
            this.textBoxErgebnis.Name = "textBoxErgebnis";
            this.textBoxErgebnis.ReadOnly = true;
            this.textBoxErgebnis.Size = new System.Drawing.Size(606, 116);
            this.textBoxErgebnis.TabIndex = 11;
            this.textBoxErgebnis.Text = "0";
            this.textBoxErgebnis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxErgebnis.Visible = false;
            // 
            // ParamDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 480);
            this.Controls.Add(this.textBoxErgebnis);
            this.Controls.Add(this.labelLetztesErgebnis);
            this.Controls.Add(this.labelReizZahl);
            this.Controls.Add(this.numericUpDownAnzahlReize);
            this.Controls.Add(this.labelReizDauerEinheit);
            this.Controls.Add(this.labelReizDauer);
            this.Controls.Add(this.numericUpDownReizDauer);
            this.Controls.Add(this.labelN);
            this.Controls.Add(this.numericUpDownN);
            this.Controls.Add(this.labelProband);
            this.Controls.Add(this.textBoxProbandenName);
            this.Controls.Add(this.buttonStarteTest);
            this.Name = "ParamDialog";
            this.Text = "nBack";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownReizDauer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnzahlReize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStarteTest;
        private System.Windows.Forms.TextBox textBoxProbandenName;
        private System.Windows.Forms.Label labelProband;
        private System.Windows.Forms.NumericUpDown numericUpDownN;
        private System.Windows.Forms.Label labelN;
        private System.Windows.Forms.Label labelReizDauer;
        private System.Windows.Forms.NumericUpDown numericUpDownReizDauer;
        private System.Windows.Forms.Label labelReizDauerEinheit;
        private System.Windows.Forms.Label labelReizZahl;
        private System.Windows.Forms.NumericUpDown numericUpDownAnzahlReize;
        private System.Windows.Forms.Label labelLetztesErgebnis;
        private System.Windows.Forms.TextBox textBoxErgebnis;
    }
}

