﻿namespace NBack
{
    partial class TestDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.lbl_Buchstabe = new System.Windows.Forms.Label();
            this.btn_Wiederholung = new System.Windows.Forms.Button();
            this.btn_KeineWiederholung = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Cancel.Location = new System.Drawing.Point(370, 249);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 0;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            // 
            // lbl_Buchstabe
            // 
            this.lbl_Buchstabe.AutoSize = true;
            this.lbl_Buchstabe.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Buchstabe.Location = new System.Drawing.Point(125, 76);
            this.lbl_Buchstabe.Name = "lbl_Buchstabe";
            this.lbl_Buchstabe.Size = new System.Drawing.Size(218, 46);
            this.lbl_Buchstabe.TabIndex = 1;
            this.lbl_Buchstabe.Text = "Buchstabe";
            // 
            // btn_Wiederholung
            // 
            this.btn_Wiederholung.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Wiederholung.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Wiederholung.Location = new System.Drawing.Point(12, 249);
            this.btn_Wiederholung.Name = "btn_Wiederholung";
            this.btn_Wiederholung.Size = new System.Drawing.Size(98, 23);
            this.btn_Wiederholung.TabIndex = 2;
            this.btn_Wiederholung.Text = "Wiederholung";
            this.btn_Wiederholung.UseVisualStyleBackColor = true;
            this.btn_Wiederholung.Click += new System.EventHandler(this.btn_Wiederholung_Click);
            // 
            // btn_KeineWiederholung
            // 
            this.btn_KeineWiederholung.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_KeineWiederholung.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_KeineWiederholung.Location = new System.Drawing.Point(145, 249);
            this.btn_KeineWiederholung.Name = "btn_KeineWiederholung";
            this.btn_KeineWiederholung.Size = new System.Drawing.Size(95, 23);
            this.btn_KeineWiederholung.TabIndex = 3;
            this.btn_KeineWiederholung.Text = "Weiter";
            this.btn_KeineWiederholung.UseVisualStyleBackColor = true;
            this.btn_KeineWiederholung.Click += new System.EventHandler(this.btn_KeineWiederholung_Click);
            // 
            // TestDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 284);
            this.Controls.Add(this.btn_KeineWiederholung);
            this.Controls.Add(this.btn_Wiederholung);
            this.Controls.Add(this.lbl_Buchstabe);
            this.Controls.Add(this.btn_Cancel);
            this.Name = "TestDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TestDialog";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TestDialog_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label lbl_Buchstabe;
        private System.Windows.Forms.Button btn_Wiederholung;
        private System.Windows.Forms.Button btn_KeineWiederholung;
    }
}