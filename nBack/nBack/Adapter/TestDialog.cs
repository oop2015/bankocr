﻿using System;
using System.Windows.Forms;

namespace NBack
{
    public partial class TestDialog : Form
    {
        public TestDialog()
        {
            this.InitializeComponent();
        }

        public event Action<Antworten> AntwortGegeben; 

        public void ZeigeFrage(Char frage)
        {
            if (!this.Visible)
            {
                this.Show();
            }

            this.lbl_Buchstabe.Text = frage.ToString();
        }

        public void Beenden()
        {
            this.Visible = false;
        }

        private void TestDialog_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsWhiteSpace(e.KeyChar))
            {
                this.AntwortGegeben(Antworten.Wiederholung);
            }
        }

        private void btn_Wiederholung_Click(object sender, EventArgs e)
        {
            this.AntwortGegeben(Antworten.Wiederholung);
        }

        private void btn_KeineWiederholung_Click(object sender, EventArgs e)
        {
            this.AntwortGegeben(Antworten.KeineWiederholung);
        }
    }
}
