﻿using System;
using System.Windows.Forms;

namespace NBack
{
    public partial class ParamDialog : Form
    {
        public ParamDialog()
        {
            this.InitializeComponent();
        }

        public event Action<String, UInt32, UInt32, UInt32> ParameterEingegeben; 

        public void Starten()
        {
            Application.Run(this);
        }

        public void ZeigeErgebnis(Ergebnis ergebnis)
        {
            this.textBoxErgebnis.Text = ergebnis.ProzentRichtig.ToString();
            this.textBoxErgebnis.Visible = true;
            this.labelLetztesErgebnis.Visible = true;
        }

        private void buttonStarteTest_Click(object sender, EventArgs e)
        {
            var tempEvent = this.ParameterEingegeben;
            if (tempEvent != null)
            {
                tempEvent(this.textBoxProbandenName.Text, Convert.ToUInt32(this.numericUpDownN.Value), Convert.ToUInt32(this.numericUpDownReizDauer.Value), Convert.ToUInt32(this.numericUpDownAnzahlReize.Value));
            }
        }
    }
}
